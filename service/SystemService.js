var Promise = require('bluebird');

function SystemService(model) {
    this._model = model;
    this.isSystemUser = function (userId) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self._model.find({
                where: {
                    id: userId,
                    role: 'System'
                }
            }).then(function (user) {
                if (user && user.role == 'System') {
                    return resolve(true);
                } else {
                    return resolve(false);
                }
            }).catch(function (err) {
                console.log('SYSTEM USER CHECK FAIL ERROR', userId, err);
                return resolve(false);
            })
        });
    };
    return this;
};

module.exports = SystemService;