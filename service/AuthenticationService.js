var SecurityService = require('./SecurityService');

function AuthenticationService() {
    this._securityService = new SecurityService();

    this.authenticateAdmin = function (event) {
        const payload = this.authenticateAll(event);

        if (payload.role != 'Admin' && payload.role != 'System') {
            return false;
        }

        return payload.id;
    };

    this.authenticateSystem = function (event) {
        const payload = this.authenticateAll(event);

        if (payload.role != 'System') {
            return false;
        }

        return payload.id;
    };

    this.authenticateMember = function (event) {
        const payload = this.authenticateAll(event);

        if (payload.role == 'Vendor') {
            return false;
        }

        return payload.id;
    };

    this.authenticateVendor = function (event) {
        const payload = this.authenticateAll(event);

        if (payload.role != 'Vendor') {
            return false;
        }

        return payload.id;
    };

    this.authenticateAll = function (event) {
        const authorization = event.headers && event.headers.Authorization ? event.headers.Authorization : false;

        if (!authorization) {
            return false;
        }

        const payload = this._securityService.verifyAccessToken(authorization);

        if (!payload) {
            return false;
        }
        return payload;
    };
    return this;
};

module.exports = AuthenticationService;