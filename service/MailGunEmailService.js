var Mailgun = require('mailgun-js');

function MailGunEmailService() {
    this._publicAPIKey = process.env.MAILGUN_PUBLIC_API_KEY;
    this._secretAPIKey = process.env.MAILGUN_SECRET_API_KEY;
    this._fromEmail = process.env.MAILGUN_FROM_EMAIL;
    this._domain = process.env.MAILGUN_DOMAIN;
    this._transporter = new Mailgun({
        apiKey: this._secretAPIKey,
        domain: this._domain
    });

    this.sentEmail = function (to, subject, html, payload, cb) {
        // setup e-mail data with unicode symbols
        var data = {
            from: this._fromEmail, // sender address
            to: to, // list of receivers
            subject: this.processPayload(subject, payload), // Subject line
            html: this.processPayload(html, payload) // html body
        };

        //Invokes the method to send emails given the above data with the helper library
        this._transporter.messages().send(data, function (err, body) {
            //If there is an error, render the error page
            if (err) {
                console.log('Sent Email Error: ', err);
                cb(err);
            } else {
                console.log('Email Content ', body);
                cb(null, true);
            }
        });
    };

    this.processPayload = function (text, payload) {
        const keys = Object.keys(payload);
        for (var i = 0; i < keys.length; i++) {
            text = text.replace(new RegExp('{{' + keys[i] + '}}', 'g'), payload[keys[i]]);
        }
        return text;
    };
};

module.exports = MailGunEmailService;