var AWS = require('aws-sdk');
var Promise = require('bluebird');

function SNSService(dispatchFunction) {
    this._region = process.env.AWS_REGION;
    this._accountId = process.env.AWS_ACCOUNTID;
    this._dispatchFunction = dispatchFunction;
    this._notificationService = new AWS.SNS({ apiVersion: '2010-03-31' });
    this._constructMessagePayload = function (payload) {
        return {
            Message: JSON.stringify(payload),
            TopicArn: 'arn:aws:sns:' + this._region + ':' + this._accountId + ':' + this._dispatchFunction
        };
    };

    this.deliverMessage = function (payload) {
        var self = this;
        return new Promise(function (resolve, reject) {
            const msg = self._constructMessagePayload(payload);
            console.log(msg);
            return self._notificationService.publish(msg,
                function (error, data) {
                    if (error) {
                        console.log('SNS Failed ', error);
                        return reject(error);
                    } else {
                        return resolve({
                            message: 'Message successfully',
                            params: payload,
                            data: data
                        });
                    }
                });
        });
    };
    return this;
}

module.exports = SNSService;