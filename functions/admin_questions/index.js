'use strict';
var iocContainer = require('./ioContainer');
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();

exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    iocContainer.models.questions.findAll({}).then( function (question) {
            return firebaseService.write('question', 1, question.map(function(oneQuestion){ return oneQuestion.toJSON();})).then(function(result){
                return callback(null, question);
            });        
        }).catch( function (e) {
            console.log('Question GET ERROR', e);
            callback(null, {
                statusCode: 404,
                message: 'Preguntas no encontradas'
            });
        });
}
