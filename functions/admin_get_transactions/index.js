'use strict';
var iocContainer = require('./ioContainer');
var lodash = require('lodash');
var PaginationService = require('./service/PaginationService');
exports.handle = function (event, context, callback) {
    const userId = event.principalId;
    context.callbackWaitsForEmptyEventLoop = false;
    const pagination = new PaginationService(event.query.page, event.query.numItems);

    const date = event && event.query && event.query.date ? event.query.date : null;
    const action = event && event.query && event.query.action ? event.query.action : null;
    const amount = event && event.query && event.query.amount ? event.query.amount : null;

    let where = {};
    let query = {
        attributes: [[iocContainer.db.fn('COUNT', iocContainer.db.col('id')), 'num']],
        order: [
            ['id', 'ASC']
        ]
    };


    if (date) {
        where.created_at = date;
    }
    if (action) {
        where.action = action;
    }
    if (amount) {
        where.amount = amount;
    }

    if (!lodash.isEmpty(where)) {
        query.where = where;
    }
    iocContainer.models.transactions.findAll(query).then(
        function (countResult) {
            if (countResult.length > 0) {
                pagination.setCount(countResult[0].dataValues.num);
                delete query.attributes;
                query.offset = pagination.getOffset();
                query.limit = pagination.getLimit();
                return iocContainer.models.transactions.findAll(query);
            } else {
                callback(null, {
                    statusCode: 200,
                    items: pagination.getItems(),
                    page: pagination.getPage(),
                    num_items: pagination.getCount(),
                    num_pages: pagination.getNumPages()
                });
            }
        }).then(function (transactions) {
            pagination.setItems(transactions);
            callback(null, {
                statusCode: 200,
                items: pagination.getItems().map(function (transaction) {
                    transaction.created_at = transaction.created_at.toISOString().slice(0, 10);
                    return transaction;
                }),
                page: pagination.getPage(),
                num_items: pagination.getCount(),
                num_pages: pagination.getNumPages()
            });
        }).catch(function (e) {
            console.log('transactions GET ERROR', e);
            callback(null, {
                statusCode: 404,
                message: 'Transacción no encontrada'
            });
        });
}
