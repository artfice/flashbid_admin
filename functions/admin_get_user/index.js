'use strict';
 var iocContainer = require('./ioContainer');
 var lodash = require('lodash');
 var PaginationService = require('./service/PaginationService');
 exports.handle = function(event, context, callback) { 
        context.callbackWaitsForEmptyEventLoop = false;
        const pagination = new PaginationService(event.query.page, event.query.numItems);
        const email = event && event.query && event.query.email ? event.query.email : null;
        const phone_number = event && event.query && event.query.phone_number ? event.query.phone_number : null;
        const first_name = event && event.query && event.query.first_name ? event.query.first_name : null;
        const last_name = event && event.query && event.query.last_name ? event.query.last_name : null;
        const role = event && event.query && event.query.role ? event.query.role : null;
        const status = event && event.query && event.query.status ? event.query.status : null;
        const id = event && event.query && event.query.id ? event.query.id : null;
        const date = event && event.query && event.query.date ? event.query.date : null;

        let where = {};
        let query = {
            attributes: [[iocContainer.db.fn('COUNT', iocContainer.db.col('id')), 'num']],
            order: [
                ['id', 'DESC']
            ]
        };

        if (date) {
            where.created_at = date;
        }
        if (id) {
            where.id = id;
        }

        if (email) {
            where.email = email;
        }
        if (phone_number) {
            where.phone_number = phone_number;
        }
        if (first_name) {
            where.first_name = first_name;
        }
        if (last_name) {
            where.last_name = last_name;
        }
        if (role) {
            where.role = role;
        }
        if (status) {
            where.status = status;
        }

        if (!lodash.isEmpty(where)) {
            query.where = where;
        }
        iocContainer.models.users.findAll(query).then(
            function (countResult) {
                if (countResult.length > 0) {
                    pagination.setCount(countResult[0].dataValues.num);
                    delete query.attributes;
                    query.offset = pagination.getOffset();
                    query.limit = pagination.getLimit();
                    return iocContainer.models.users.findAll(query);
                } else {
                    callback(null, {
                        statusCode: 200,
                        items: pagination.getItems(),
                        page: pagination.getPage(),
                        num_items: pagination.getCount(),
                        num_pages: pagination.getNumPages()
                    });
                }
            }).then( function (users) {
                pagination.setItems(users);
                callback(null, {
                    statusCode: 200,
                    items: pagination.getItems().map(function(item){
                        item = item.toJSON();
                        item.created_at = item.created_at.toISOString().slice(0, 10);
                        delete item.access_token;
                        delete item.refresh_token;
                        delete item.confirm_token;
                        delete item.reset_token;
                        delete item.term_condition;
                        delete item.password;
                        return item;
                    }),
                    page: pagination.getPage(),
                    num_items: pagination.getCount(),
                    num_pages: pagination.getNumPages()
                });
        }).catch( function (e) {
            console.log('User GET ERROR', e);
            callback(null, {
                statusCode: 404,
                message: 'Usuario no encontrado'
            });
        });     
}
