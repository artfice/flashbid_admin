'use strict';
var iocContainer = require('./ioContainer');
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();

exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;

    const body = event.body;
    const id = event.body.id;
    const subject = body && body.subject ? body.subject : '';
    const html = body && body.html ? body.html : '';
    let updateFields = {};
    if (subject.length > 0) {
        updateFields.subject = subject;
    }
    
    if (html.length > 0) {
        updateFields.html = html;
    }

    iocContainer.models.emails.find({
        where: {
            id: id
        }
    }).then(function (email) {
        return email.updateAttributes(updateFields);
    }).then(function (email) {
        return callback(null, email);
    }).catch(function (e) {
        console.log('email GET ERROR', e);
        callback(null, {
            statusCode: 403,
            message: 'Un Fallo en la actualizacion de la plantilla de correo electronico.'
        });
    });
}
