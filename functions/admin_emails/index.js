'use strict';
var iocContainer = require('./ioContainer');
exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    iocContainer.models.emails.findAll({}).then( function (email) {
            callback(null, email);
        }).catch( function (e) {
            console.log('email GET ERROR', e);
            callback(null, {
                statusCode: 404,
                message: 'Correos electrónicos no encontrados'
            });
        });
}
