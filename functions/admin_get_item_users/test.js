var index = require('./index');

index.handle({
    principalId: 4,
    query: {
        page: 1,
        id: 32,
        numItems: 10
    }
}, {
        callbackWaitsForEmptyEventLoop: false
    }, function (e, data) {
        console.log(e);
        console.log(data);
    })