'use strict';
var iocContainer = require('./ioContainer');
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();

exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    iocContainer.models.about.find({
        where: {
            id: 1
        }
    }).then( function (about) {
        return firebaseService.write('about', 1, about.toJSON()).then(function(result){
            return callback(null, about);
        });
    }).catch( function (e) {
        console.log('about GET ERROR', e);
        callback(null, {
            statusCode: 404,
            message: 'Tema no encontrado'
        });
    });
}
