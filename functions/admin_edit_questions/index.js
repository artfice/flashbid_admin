'use strict';
var iocContainer = require('./ioContainer');
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();

exports.handle = function (event, context, callback) {
            context.callbackWaitsForEmptyEventLoop = false;
        const body = event.body;
        const id = event.body.id;
        const rawQuestion = body && body.question ? body.question : ''; 
        const rawAnswer = body && body.answer ? body.answer : '';
        iocContainer.models.questions.find({
            where: {
            id: id
            }
        }).then( function (question) {
            if(question && rawQuestion.length > 0 && rawAnswer.length > 0){
                return question.updateAttributes({
                    question: rawQuestion,
                    answer: rawAnswer
                });
            } else {
                throw new Error('question is empty');
            }            
        }).then(function (question) {
            return callback(null, question);
        }).catch( function (e) {
            console.log('Questions PUT ERROR', e);
            callback(null, {
                    statusCode: 403,
                    message: 'La pregunta y la respuesta no pueden estar vacías'
                });
        });
}
