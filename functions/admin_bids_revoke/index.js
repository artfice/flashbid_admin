'use strict';
var iocContainer = require('./ioContainer');
var SystemService = require('./service/SystemService');
exports.handle = function (event, context, callback) {
    const userId = event.principalId;
    context.callbackWaitsForEmptyEventLoop = false;
    const id = event.query.id;
    let isSystem = true;
    const systemService = new SystemService(iocContainer.models.users);

    systemService.isSystemUser(userId).then(function (isSystemResult) {
        console.log(isSystemResult);
        if (!isSystemResult) {
            isSystem = false;
            throw new Error('Not System User');
            return false;
        } else {
            return iocContainer.models.bid_cards.find({
                where: {
                    id: id
                }
            });
        }
    }).then(function (bidCard) {
        if (bidCard.status == 0) {
            return bidCard.updateAttributes({
                status: 2,
                update_at: new Date()
            });
        } else {
            return {
                statusCode: 403,
                message: 'Cannot revoke this bid card'
            };
        }
    }).then(function (bidCard) {
        return callback(null, bidCard);
    }).catch(function (e) {
        if (!isSystem) {
            return callback(null, {
                statusCode: 403,
                message: 'You are not authorize to do this task'
            });
        } else {
            console.log('Change Bonus ERROR', e);
            return callback(e);
        }
    });
}
