'use strict';
var iocContainer = require('./ioContainer');
var SecurityService = require('./service/SecurityService');
var lodash = require('lodash');
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();

exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    const securityService = new SecurityService();
    const body = event.body;
    const email = body && body.email ? body.email : null;
    const password = body && body.password ? body.password : null;
    let accessToken = '';
    let refreshToken = '';

    if (!email || email.length < 1) {
        return callback(null, {
            statusCode: 403,
            message: 'Falta el correo electrónico'
        });
    }
    if (!password || password.length < 1) {
        return callback(null, {
            statusCode: 403,
            message: 'Falta la contraseña'
        });
    }

    iocContainer.models.users.find({
        where: {
            email: email
        }
    }).then(function (user) {
        const oldPassword = user.password;
        if (!securityService.compareHash(oldPassword, password)) {
            return {
                statusCode: 403,
                message: 'Tu contraseña es incorrecta'
            };
        } else if (user.status == 'Suspend') {
            return {
                statusCode: 403,
                message: 'Tu cuenta ha sido suspendida.'
            };
        } else if (user.status == 'Ban') {
            return {
                statusCode: 403,
                message: 'Su cuenta ha sido prohibida.'
            };
        } else if (user.status == 'Pending') {
            return {
                statusCode: 403,
                message: 'Confirme su cuenta antes de iniciar sesión.'
            };
        } else if (user.role == 'Member' || user.role == 'Vendor') {
            throw new Error('[401] Unauthorized Access');
        } else {
            accessToken = securityService.createAccessToken({
                id: user.id,
                role: user.role,
                status: user.status
            }, process.env.ACCESS_TOKEN_EXPIRE_SECOND);

            return user.updateAttributes({
                access_token: accessToken,
                refresh_token: ''
            });
        }
    }).then(function (user) {
        if (user.statusCode) {
            return callback(null, user);
        } else {
            user = user.toJSON();
            const access_token = user.access_token;
            delete user.access_token;
            delete user.refresh_token;
            delete user.confirm_token;
            delete user.reset_token;
            delete user.term_condition;
            delete user.password;
            delete user.role;
            return callback(null, {
                access_token: access_token,
                refresh_token: '',
                user_id: user.id
            });
        }
    }).catch(function (e) {
        console.log('Member Login GET ERROR', e);
        callback(null, {
           statusCode: 403,
           message: 'credenciales no válidas' 
        });
    });

};