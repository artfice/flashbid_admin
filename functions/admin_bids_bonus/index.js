'use strict';
var iocContainer = require('./ioContainer');
var lodash = require('lodash');
var SystemService = require('./service/SystemService');
exports.handle = function (event, context, callback) {
    const userId = event.principalId;
    context.callbackWaitsForEmptyEventLoop = false;
    const id = event.body.id;
    const bonus = event.body.bonus ? event.body.bonus : 0;
    let isSystem = true;
    let cannotChangeBonus = false;
    const systemService = new SystemService(iocContainer.models.users);
    let currentBidCard = null;
    let originalBidBonus = 0;
    systemService.isSystemUser(userId).then(function (isSystemResult) {
        if (!isSystemResult) {
            isSystem = false;
            throw new Error('Not System User');
        } else {
            return iocContainer.models.bid_cards.find({
                where: {
                    id: id
                }
            });
        }
    }).then(function (bidCard) {
        originalBidBonus = bidCard.bonus;
        if (bidCard.status == 0) {
            return bidCard.updateAttributes({
                bonus: bonus,
                update_at: new Date()
            });
        } else {
            cannotChangeBonus = true;
            throw new Error('Cannont change balance');
        }
    }).then(function (bidCard) {
        currentBidCard = lodash.clone(bidCard);
        return iocContainer.models.system.find({
            where: {
                id: 1
            }
        }); 
    }).then(function (system) {               
        return system.updateAttributes({
                bonus_bid_card: system.bonus_bid_card - originalBidBonus + bonus
        });        
    }).then(function (system) {                       
        return callback(null, currentBidCard);
    }).catch(function (e) {
        if (!isSystem) {
            return callback(null, {
                statusCode: 403,
                message: 'You are not authorize to do this task'
            });
        } else if (cannotChangeBonus) {
            return callback(null, {
                statusCode: 403,
                message: 'Cannot change the bonus'
            });
        } else {
            console.log('Change Bonus ERROR', e);
            callback(e);
        }
    });
}
