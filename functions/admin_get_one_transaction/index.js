'use strict';
var iocContainer = require('./ioContainer');
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();

exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    const id = event.path.id;
    iocContainer.models.transactions.find({
        where: {
            id: id
        }
    }).then( function (transaction) {
        return callback(null, transaction);
    }).catch( function (e) {
        console.log('transaction GET ERROR', e);
        callback(null, {
            statusCode: 404,
            message: 'Transacción no encontrada'
        });
    });
}
