'use strict';
var iocContainer = require('./ioContainer');
var SecurityService = require('./service/SecurityService');
var SNSService = require('./service/SNSService');
exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    const securityService = new SecurityService();
    const body = event.body;
    const first_name = body && body.first_name ? body.first_name : '';
    const last_name = body && body.last_name ? body.last_name : '';
    const email = body && body.email ? body.email : '';
    const password = body && body.password ? body.password : '';
    const profile_image = body && body.profile_image ? body.profile_image : 'https://s3.amazonaws.com/images.flashbid/profile.png';
    const phone_number = body && body.phone_number ? body.phone_number : 0;

    if (first_name.length < 1) {
        return callback(null, {
            statusCode: 403,
            message: 'Nombre es requerido.'
        });
    }
    if (last_name.length < 1) {
        return callback(null, {
            statusCode: 403,
            message: 'Apellido es requerido'
        });
    }
    if (email.length < 1) {
        return callback(null, {
            statusCode: 403,
            message: 'Ingrese un correo electrónico Valido'
        });
    }
    if (password.length < 8) {
        return callback(null, {
            statusCode: 403,
            message: 'Su contraseña debe contener una mayúscula, símbolo, numero y por lo menos 8 letras o y caracteres.'
        });
    }
    if (phone_number.length < 8) {
        return callback(null, {
            statusCode: 403,
            message: 'Numero de telefono es invalido.'
        });
    }

    iocContainer.models.users.create({
        first_name: first_name,
        last_name: last_name,
        email: email,
        phone_number: phone_number,
        profile_image: profile_image,
        password: securityService.hash(password),
        balance: 0,
        term_condition: 1,
        status: 'Pending',
        role: 'Member',
        access_token: '',
        refresh_token: '',
        confirm_token: securityService.generateString(20),
        bid_attempts: 0,
        money_owed: 0,
        group_name: '',
        created_at: new Date(),
        update_at: new Date()
    }).then(function (user) {
        const snsService = new SNSService('sendEmail');
        return snsService.deliverMessage({
            template: 'register',
            user_id: user.id,
            data: {
                confirm_token: user.confirm_token
            }
        }).then(function (msg) {
            return callback(null, user);
        });
    }).catch(function (e) {
        console.log('User POST ERROR', e);
        if (e.name == 'SequelizeUniqueConstraintError') {
            callback(null, {
                statusCode: 403,
                message:  'Este correo electronico ya existe en nuestro sistema.'
            })
        } else {
            callback(null, {
                statusCode: 403,
                message: 'Un fallo en la creacion del usuario.'
            });
        }
    });
}
