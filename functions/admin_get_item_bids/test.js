var index = require('./index');

index.handle({
    principalId: 4,
    query: {
        page: 1,
        id: 34,
        numItems: 3
    }
}, {
        callbackWaitsForEmptyEventLoop: false
    }, function (e, data) {
        console.log(e);
        console.log(data);
    })