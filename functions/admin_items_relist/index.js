'use strict';
var iocContainer = require('./ioContainer');
var SecurityService = require('./service/SecurityService');
var SNSService = require('./service/SNSService');
var FirebaseService = require('./service/FirebaseService');
var SystemService = require('./service/SystemService');
var firebaseService = new FirebaseService();
const securityService = new SecurityService();

exports.handle = function (event, context, callback) {
    const userId = event.principalId;
    context.callbackWaitsForEmptyEventLoop = false;
    const id = event.path.id;

    iocContainer.models.items.find({
        where: {
            id: id
        }
    }).then(function (item) {
        if (item) {
            var itemJSON = item.toJSON();
            itemJSON.final_bid = 0;
            itemJSON.room_id = 0;
            itemJSON.winner_id = 0;
            itemJSON.ticket_sold = 0;
            itemJSON.winner_code = securityService.generateString(8);
            itemJSON.status = 'Draft';
            itemJSON.created_at = new Date();
            itemJSON.update_at = new Date();
            delete itemJSON.id;
            return iocContainer.models.items.create(itemJSON);
        } else {
            throw new Error('El articulo no se relistó.');
        }
    }).then(function (item) {
        return callback(null, item);
    }).catch(function (e) {
        console.log('ITEM POST ERROR', e);
        callback(null, {
            statusCode: 403,
            message: 'El articulo no se relistó.'
        });
    });
}
