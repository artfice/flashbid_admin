'use strict';
var iocContainer = require('./ioContainer');
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();

exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    iocContainer.models.terms.find({
        where: {
            id: 1
        }        
    }).then( function (terms) {
        return firebaseService.write('terms', 1, terms.toJSON()).then(function(result){
            return callback(null, terms);
        });         
            
        }).catch( function (e) {
            console.log('terms GET ERROR', e);
            callback(null, {
                statusCode: 404,
                message: 'Términos y condiciones no encontradas'
            });
        });
}
