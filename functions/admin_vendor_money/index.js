'use strict';
var iocContainer = require('./ioContainer');
var lodash = require('lodash');
var SecurityService = require('./service/SecurityService');
var SNSService = require('./service/SNSService');
var FirebaseService = require('./service/FirebaseService');
var SystemService = require('./service/SystemService');
var firebaseService = new FirebaseService();
function updateUser(updateFields, id) {
    return iocContainer.models.users.update(updateFields, {
        where: {
            id: id
        }
    });
}

exports.handle = function (event, context, callback) {
    const userId = event.principalId;
    const today = new Date();
    const todayStr = today.toISOString().slice(0, 10);
    context.callbackWaitsForEmptyEventLoop = false;
    const securityService = new SecurityService();
    const body = event.body;
    const id = event.path.id;
    const amount = body && body.amount ? body.amount : 0;
    let updateFields = {};
    let currentUser = null;
    let currentSystem = null;
    let oldBalance = 0;
    let newBalance = 0;
    let balanceDifference = 0;
    let userNotFound = false;

    if (amount > 0) {
        updateFields.amount = amount;
    }
    iocContainer.models.users.find({
        where: {
            id: id,
            role: 'Vendor'
        }
    }).then(function (user) {
        if (user) {
            currentUser = lodash.clone(user);
            oldBalance = currentUser.balance;
            if (amount > oldBalance) {
                balanceDifference = 0;
            } else {
                balanceDifference = oldBalance - amount;
            }

            return user.updateAttributes({
                money_owed: balanceDifference
            });
        } else {
            userNotFound = true;
            throw new Error('User cannot be found');
        }
    }).then(function (user) {
        return iocContainer.models.system.find({
            where: {
                id: 1
            }
        });
    }).then(function (system) {
        currentSystem = lodash.clone(system);
        return iocContainer.models.transactions.create({
            action: 'VendorPaid',
            user_id: id,
            item_id: 0,
            amount: (amount > oldBalance) ? oldBalance : amount,
            userOldTotal: oldBalance,
            userTotal: (oldBalance - amount > 0) ? (oldBalance - amount) : 0,
            systemOldTotal: currentSystem.earned,
            systemTotal: currentSystem.earned,
            code: '',
            notes: 'Vendedor Bid Autorizado ' + id + ' realizo un pago a Flashbid de monedas vendidas a usuarios de ' + (amount),
            created_at: todayStr,
            updated_at: today
        });
    }).then(function (system) {
        return currentSystem.updateAttributes({
            money_claimed: currentSystem.money_claimed + amount
        });
    }).then(function (system) {
        return callback(null, {
            statusCode: 200,
            message: 'Actualización del saldo del Vendedor Flash Autorizado'
        });
    }).catch(function (e) {
        if (userNotFound) {
            return callback(null, {
                statusCode: 403,
                message: 'Vendedor no encontrado'
            });
        } else {
            console.log('User POST ERROR', e);
            return callback(null, {
                statusCode: 403,
                message: 'Vendedor no encontrado'
            });
        }
    })



}
