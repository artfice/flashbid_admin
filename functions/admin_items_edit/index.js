'use strict';
var iocContainer = require('./ioContainer');
var SecurityService = require('./service/SecurityService');
var SNSService = require('./service/SNSService');
var FirebaseService = require('./service/FirebaseService');
var SystemService = require('./service/SystemService');
var firebaseService = new FirebaseService();

exports.handle = function (event, context, callback) {
    const userId = event.principalId;
    context.callbackWaitsForEmptyEventLoop = false;
    const securityService = new SecurityService();
    const body = event.body;
    const id = event.path.id;
    const title = body && body.title ? body.title : '';
    const description = body && body.description ? body.description : '';
    const winner_instructions = body && body.winner_instructions ? body.winner_instructions : '';
    const size = body && body.size ? body.size : '';
    const image = body && body.image ? body.image : '';
    const secondary_image = body && body.secondary_image ? body.secondary_image : '';
    const type = body && body.type ? body.type : '';
    const intial_bid = body && body.intial_bid ? body.intial_bid : undefined;
    const item_worth = body && body.item_worth ? body.item_worth : undefined;
    const vendor_id = body && body.vendor_id ? body.vendor_id : undefined;
    const bid_amount = body && body.bid_amount ? body.bid_amount : 0;
    const ticket_amount = body && body.ticket_amount ? body.ticket_amount : 0;

    let updateFields = {};

    if (title.length > 0) {
        updateFields.title = title;
    }

    if (secondary_image.length > 0) {
        updateFields.secondary_image = secondary_image;
    }

    if (description.length > 0) {
        updateFields.description = description;
    }

    if (size.length > 0) {
        updateFields.size = size;
    }

    if (vendor_id > 0) {
        updateFields.vendor_id = vendor_id;
    }

    if (bid_amount > 0) {
        updateFields.bid_amount = bid_amount;
    }

    if (ticket_amount > 0) {
        updateFields.ticket_amount = ticket_amount;
    }

    if (winner_instructions.length > 0) {
        updateFields.winner_instructions = winner_instructions;
    }

    if (image.length > 0) {
        updateFields.image = image;
    }

    if (type.length > 0) {
        updateFields.type = type;
    }

    if (intial_bid != undefined) {
        updateFields.intial_bid = intial_bid;
    }

    // if (item_worth != undefined) {
    //     updateFields.item_worth = item_worth;
    // }

    iocContainer.models.items.find({
        where: {
            id: id
        }
    }).then(function (item) {
        if (item) {
            let new_item_worth = item.item_worth;

            if (ticket_amount) {
                const old_ticket_total = item.ticket_total;
                const ticket_sold = item.ticket_sold;

                const new_ticket_total = Math.ceil(new_item_worth / ticket_amount);
                const new_ticket_left = new_ticket_total - ticket_sold;

                if (new_ticket_left < 1) {
                    new_ticket_total = ticket_sold + 1;
                } 

                updateFields.ticket_total = new_ticket_total;
                updateFields.ticket_amount = ticket_amount;
                //100 new_item_worth
                //10 ticket_amount
                //10 tickets
                //we sell 5 tickets
                //ticket sold 5
                //5 tickets ticket_left
                // user changes ticket amount  20
                //new total =  100 / 20 == 5
                // new ticket left = 0

                // user changes ticket amount to 30
                //new total = 100 / 30 = 3
                // new ticket left = -2

                // user changes ticket amount to 5
                //new total = 100 / 5 = 20
                //new ticket left = 15
                const ticket_total = Math.ceil(item_worth / ticket_amount);
            }
            return item.updateAttributes(updateFields);
        } else {
            throw new Error('Item not found');
        }
            
    }).then(function (item) {
        if (item) {
            const snsService = new SNSService('refreshItems');
            return snsService.deliverMessage({}).then(function (msg) {
                return callback(null, item);
            }).catch(function (err) {
                console.log('Refresh Room Error SNS ', err);
                return callback(null, item);
            });
        } else {
            return callback(null, {
                statusCode: 403,
                message: 'Error en el elemento de edición'
            });
        }
    }).catch(function (e) {
        console.log('ITEM POST ERROR', e);
        callback(null, {
            statusCode: 403,
            message: 'Error en el elemento de edición'
        });
    });
}
