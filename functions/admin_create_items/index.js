'use strict';
var iocContainer = require('./ioContainer');
var SecurityService = require('./service/SecurityService');
var SNSService = require('./service/SNSService');
exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    const securityService = new SecurityService();
    const body = event.body;
    const title = event && event.body && event.body.title ? event.body.title : null;
    const description = event && event.body && event.body.description ? event.body.description : null;
    const image = event && event.body && event.body.image ? event.body.image : null;
    const secondary_image = event && event.body && event.body.secondary_image ? event.body.secondary_image : null;
    const initial_bid = event && event.body && event.body.initial_bid ? event.body.initial_bid : 0;
    const bid_amount = event && event.body && event.body.bid_amount ? event.body.bid_amount : 10;
    const buy_at_amount = event && event.body && event.body.buy_at_amount ? event.body.buy_at_amount : 0;
    const ticket_amount = event && event.body && event.body.ticket_amount ? event.body.ticket_amount : 3;
    const vendor_id = event && event.body && event.body.vendor_id ? event.body.vendor_id : 0;
    const winner_instructions = event && event.body && event.body.winner_instructions ? event.body.winner_instructions : '';
    const type = event && event.body && event.body.type ? event.body.type : 'l';
    const size = event && event.body && event.body.size ? event.body.size : 'Small';
    const win_code = securityService.generateString(8);
    const item_worth = event && event.body && event.body.item_worth ? event.body.item_worth : 0;
    const ticket_total = Math.ceil(item_worth / ticket_amount);
    
    if (ticket_total < 1) {
        ticket_total = 1;
    }

    if (title.length < 1) {
        return callback(null, {
            statusCode: 403,
            message: 'El titulo es obligatorio'
        });
    }
    if (winner_instructions.length < 1) {
        return callback(null, {
            statusCode: 403,
            message: 'Faltan instrucciones del ganador'
        });
    }
    if (size.length < 1) {
        return callback(null, {
            statusCode: 403,
            message: 'Tamaño del articulo no es válido.'
        });
    }
    if (bid_amount < 1 && type == 'a') {
        return callback(null, {
            statusCode: 403,
            message: 'Precio de oferta inicial inválido.'
        });
    }
    if (initial_bid < -1 && type == 'a') {
        return callback(null, {
            statusCode: 403,
            message: 'Precio de oferta inicial no válido'
        });
    }    
    if (item_worth < 1) {
        return callback(null, {
            statusCode: 403,
            message: 'Valor del articulo no puede ser Gratis.'
        });
    }
    if (type.length < 1) {
        return callback(null, {
            statusCode: 403,
            message: 'El articulo debe ser de Subasta o Lotería.'
        });
    }
    if (!description) {
        return callback(null, {
            statusCode: 403,
            message: 'La descripcion es obligatoria.'
        });
    }

    iocContainer.models.items.create({
        title: title,
        description: description,
        image: image,
        secondary_image: secondary_image,
        type: type,
        item_worth: item_worth,
        initial_bid: initial_bid,
        final_bid: 0,
        bid_amount: bid_amount,
        buy_at_amount: buy_at_amount,
        ticket_amount: ticket_amount,
        ticket_total: ticket_total,
        ticket_sold: 0,
        winner_id: 0,
        room_id: 0,
        vendor_id: vendor_id,
        winner_code: win_code,
        winner_instructions: winner_instructions,
        size: size,
        status: 'Draft',
        created_at: new Date(),
        update_at: new Date()
    }).then(function (item) {
        const snsService = new SNSService('refreshItems');
        return snsService.deliverMessage({}).then(function (msg) {
            return callback(null, item);
        }).catch(function (err) {
            console.log('RefreshRoom Error SNS ', err);
            return callback(null, item);
        })

    }).catch(function (e) {
        console.log('Item POST ERROR', e);
        callback(null, {
            statusCode: 403,
            message: 'La creacion del articulo no pudo ser realizada.'
        });
    });
}
