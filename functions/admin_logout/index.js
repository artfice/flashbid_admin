'use strict';
var iocContainer = require('./ioContainer');
exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    const id = event.path.id;
    iocContainer.models.users.find({
        where: {
            id: id
        }
    }).then(function (user) {
        console.log(user.toJSON());
        return user.updateAttributes({
            access_token: '',
            refresh_token: ''
        });
    }).then(function (user) {
        callback(null, {
            statusCode: 200,
            message: 'Logged out'
        });
    }).catch(function (e) {
        callback(e);
    });
};