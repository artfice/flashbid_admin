'use strict';
var iocContainer = require('./ioContainer');
var SecurityService = require('./service/SecurityService');
var SNSService = require('./service/SNSService');
var FirebaseService = require('./service/FirebaseService');
var SystemService = require('./service/SystemService');
var firebaseService = new FirebaseService();
const securityService = new SecurityService();

exports.handle = function (event, context, callback) {
    const userId = event.principalId;
    context.callbackWaitsForEmptyEventLoop = false;
    const id = event.path.id;

    iocContainer.models.items.find({
        where: {
            id: id,
            status: 'Active'
        }
    }).then(function (item) {
        if (item) {
            return item.updateAttributes({
                ticket_total: item.ticket_sold - 1 < 1 ? 0 : (item.ticket_sold - 1)
            });
        } else {
            throw new Error('El elemento no se relistó');
        }
    }).then(function (item) {
            const snsService = new SNSService('auctionStart');
            return snsService.deliverMessage({
                id: id
            });        
    }).then(function (item) {
        return callback(null, {
            statusCode: 200,
            message: 'Alerta Flash!'
        });
    }).catch(function (e) {
        console.log('ITEM Flash ERROR', e);
        callback(null, {
            statusCode: 403,
            message: 'El elemento no puede ir destellando en este estado'
        });
    });
}
