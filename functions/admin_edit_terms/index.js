'use strict';
var iocContainer = require('./ioContainer');
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();

exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;

    const body = event.body;
    const description = body && body.description ? body.description : '';
    iocContainer.models.terms.find({
        where: {
            id: 1
        }
    }).then(function (term) {
        if (term && description.length > 0) {
            return term.updateAttributes({
                description: description
            });
        } else {
            throw new Error('Terms condition is empty');
        }
    }).then(function (term) {
        return firebaseService.write('terms', 1, term.toJSON()).then(function (result) {
            return callback(null, term);
        });
    }).catch(function (e) {
        console.log('term GET ERROR', e);
        callback(null, {
            statusCode: 403,
            message: 'Término y condición El texto no puede estar vacío'
        });
    });
}
