'use strict';
 var iocContainer = require('./ioContainer');
 var lodash = require('lodash');
 var FirebaseService = require('./service/FirebaseService');
 var firebaseService = new FirebaseService();

 exports.handle = function(event, context, callback) { 
     context.callbackWaitsForEmptyEventLoop = false;
     const id = event.path.id;
     const amount = event.body && event.body.amount != undefined ? event.body.amount: 0;
     let currentUser = null;
     let currentSystem = null;
     let userBalance = 0;
     const today = new Date();
     const todayStr = today.toISOString().slice(0, 10);     
    iocContainer.db.transaction({
        isolationLevel: iocContainer.isolationLevel
    }).then(function (t) {
        //STEP 1: Update User Balance
        return iocContainer.models.users.find({
            where: {
                id: id
            }
        }, { transaction: t }).then(function (user) {
            currentUser = lodash.clone(user);
            userBalance = lodash.clone(user.balance);
            
            return user.updateAttributes({
                balance: userBalance + amount
            }, { transaction: t });

        }).then(function (updatedUser) {
            var user = updatedUser.toJSON();
            delete user.access_token;
            delete user.refresh_token;
            delete user.confirm_token;
            delete user.reset_token;
            delete user.term_condition;
            delete user.role;
            delete user.password;
            return firebaseService.write('user', user.id, user).then(function(result){
                return iocContainer.models.system.find({
                    where: {
                        id: 1
                    }
                }, { transaction: t });
            });
        }).then(function (system) {
            //STEP 2: Save Transaction
            currentSystem = lodash.clone(system);
            return iocContainer.models.transactions.create({
                action: 'Refund',
                user_id: id,
                item_id: 0,
                amount: amount,
                userOldTotal: userBalance,
                userTotal: userBalance + amount,
                systemOldTotal: currentSystem.refunds,
                systemTotal: currentSystem.refunds + amount,
                notes: 'User ' + id + ' got a refund of ' + amount,
                code: '',
                created_at: todayStr,
                updated_at: today
            }, { transaction: t });
        }).then(function (transaction) {
            return iocContainer.models.system.find({
                where: {
                    id: 1
                }
            }, { transaction: t });
        }).then(function (system) {
            //STEP 3: Update System Balance
            return system.updateAttributes({
                refunds: system.refunds + amount
            }, { transaction: t });
        }).then(function (system) {
            t.commit();
            return callback(null, {
                    statusCode: 200,
                    message: 'Usuario reembolsado'
                });
        }).catch(function (err) {
            console.log('Bid Ticket Error ', err);
            t.rollback();
                return callback(null, {
                    statusCode: 403,
                    message: 'Error de reembolso del usuario'
                });
        });
    });     
}
