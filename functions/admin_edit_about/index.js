'use strict';
var iocContainer = require('./ioContainer');
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();

exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;

    const body = event.body;
    const description = body && body.description ? body.description : '';
    iocContainer.models.about.find({
        where: {
            id: 1
        }
    }).then(function (about) {
        if (about && description.length > 0) {
            return about.updateAttributes({
                description: description
            });
        } else {
            throw new Error('about is empty');
        }
    }).then(function (about) {
        return firebaseService.write('about', 1, about.toJSON()).then(function (result) {
            return callback(null, about);
        });
    }).catch(function (e) {
        console.log('ABOUT GET ERROR', e);
        callback(null, {
            statusCode: 403,
            message: 'Acerca de El texto no puede estar vacío'
        });
    });
}
