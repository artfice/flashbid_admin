'use strict';
var iocContainer = require('./ioContainer');
var SecurityService = require('./service/SecurityService');
var SNSService = require('./service/SNSService');
var FirebaseService = require('./service/FirebaseService');
var SystemService = require('./service/SystemService');
var firebaseService = new FirebaseService();

exports.handle = function (event, context, callback) {
    const userId = event.principalId;
    context.callbackWaitsForEmptyEventLoop = false;
    const securityService = new SecurityService();
    const body = event.body;
    const id = event.path.id;

    iocContainer.models.items.find({
        where: {
            id: id,
            status: 'Draft'
        }
    }).then(function (item) {
        if (item) {
            return item.updateAttributes({
                status: 'Active'
            });
        } else {
            return false;
        }
    }).then(function (item) {
        if (item) {
            const snsService = new SNSService('refreshItems');
            return snsService.deliverMessage({}).then(function (msg) {
                return callback(null, item);
            }).catch(function (err) {
                console.log('RefreshRoom Error SNS ', err);
                return callback(null, item);
            });
        } else {
            return callback(null, {
                statusCode: 403,
                message: 'Su articulo no esta en estado de borrador.'
            });
        }
    }).catch(function (e) {
        console.log('User POST ERROR', e);
        callback(null, {
            statusCode: 403,
            message: 'Su articulo no esta en estado de borrador.'
        });
    });
}
