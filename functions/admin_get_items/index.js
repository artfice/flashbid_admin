'use strict';
var iocContainer = require('./ioContainer');
var lodash = require('lodash');
var PaginationService = require('./service/PaginationService');
exports.handle = function (event, context, callback) {
    const userId = event.principalId;
    context.callbackWaitsForEmptyEventLoop = false;
    const pagination = new PaginationService(event.query.page, event.query.numItems);

    const title = event && event.query && event.query.title ? event.query.title : null;
    const initialBid = event && event.query && event.query.initial_bid ? event.query.initial_bid : null;
    const type = event && event.query && event.query.type ? event.query.type : null;
    const status = event && event.query && event.query.status ? event.query.status : null;
    const winCode = event && event.query && event.query.win_code ? event.query.win_code : null;
    const id = event && event.query && event.query.id ? event.query.id : null;
    const vendor_id = event && event.query && event.query.vendor_id ? event.query.vendor_id : null;

    let where = {};
    let query = {
        attributes: [[iocContainer.db.fn('COUNT', iocContainer.db.col('id')), 'num']],
        order: [
            ['id', 'DESC']
        ]
    };

    if (id) {
        where.id = id;
    }

    if (title) {
        where.title = title;
    }
    if (initialBid) {
        where.initialBid = initialBid;
    }
    if (type) {
        where.type = type;
    }
    if (status) {
        where.status = status;
    }
    if (winCode) {
        where.winCode = winCode;
    }
    if (vendor_id) {
        where.vendor_id = vendor_id;
    }

    if (!lodash.isEmpty(where)) {
        query.where = where;
    }
    iocContainer.models.items.findAll(query).then(
        function (countResult) {
            if (countResult.length > 0) {
                pagination.setCount(countResult[0].dataValues.num);
                delete query.attributes;
                query.offset = pagination.getOffset();
                query.limit = pagination.getLimit();
                return iocContainer.models.items.findAll(query);
            } else {
                callback(null, {
                    statusCode: 200,
                    items: pagination.getItems(),
                    page: pagination.getPage(),
                    num_items: pagination.getCount(),
                    num_pages: pagination.getNumPages()
                });
            }
        }).then(function (items) {
            pagination.setItems(items);
            callback(null, {
                statusCode: 200,
                items: pagination.getItems().map(function (item) {
                    item.created_at = item.created_at.toISOString().slice(0, 10);
                    return item;
                }),
                page: pagination.getPage(),
                num_items: pagination.getCount(),
                num_pages: pagination.getNumPages()
            });
        }).catch(function (e) {
            console.log('Items GET ERROR', e);
            callback(null, {
                statusCode: 404,
                message: 'Artículos no encontrados'
            });
        });
}
