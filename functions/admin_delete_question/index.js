'use strict';
var iocContainer = require('./ioContainer');
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();

exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
        const id = event.query.id;
        const body = event.body;
        iocContainer.models.questions.destroy({
            where: {
                id: id
            }
        }).then( function (question) {
           return callback(null, question);
        }).catch( function (e) {
            console.log('QUESTION DESTROY ERROR', e);
            callback(e);
        });
}
