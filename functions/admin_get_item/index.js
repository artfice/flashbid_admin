'use strict';
var iocContainer = require('./ioContainer');
var lodash = require('lodash');
var PaginationService = require('./service/PaginationService');
exports.handle = function (event, context, callback) {
    const userId = event.principalId;
    context.callbackWaitsForEmptyEventLoop = false;
    const id = event.path.id;
    const where = {
        id: id
    };
    let payload = {};
    iocContainer.models.items.find({ where: where }).then(
        function (item) {
            if (item) {
                callback(null, {
                    item: item
                });
            } else {
                throw new Error('Item not found');
            }
        }).catch(function (e) {
            console.log('Items GET ERROR', e);
            callback(null, {
                statusCode: 404,
                message: 'Artículos no encontrados'
            });
        });
}
