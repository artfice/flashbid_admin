'use strict';
var iocContainer = require('./ioContainer');
var SecurityService = require('./service/SecurityService');
var SNSService = require('./service/SNSService');
var FirebaseService = require('./service/FirebaseService');
var SystemService = require('./service/SystemService');
var firebaseService = new FirebaseService();

exports.handle = function (event, context, callback) {
    const userId = event.principalId;
    context.callbackWaitsForEmptyEventLoop = false;
    const securityService = new SecurityService();
    const body = event.body;
    const id = event.path.id;

    iocContainer.models.items.find({
        where: {
            id: id
        }
    }).then(function (item) {
        if (item) {
            return item.updateAttributes({
                status: 'Unlisted'
            });
        } else {
            return false;
        }
    }).then(function (item) {
        if (item) {
            const snsService = new SNSService('refreshItems');
            return snsService.deliverMessage({}).then(function (msg) {
                return callback(null, item);
            }).catch(function (err) {
                console.log('RefreshRoom Error SNS ', err);
                return callback(null, item);
            });
        } else {
            return callback(null, {
                statusCode: 403,
                message: 'No puedes cerrar este articulo.'
            });
        }
    }).catch(function (e) {
        console.log('Item POST ERROR', e);
        callback(null, {
            statusCode: 403,
            message: 'No puedes cerrar este articulo.'
        });
    });
}
