'use strict';
var iocContainer = require('./ioContainer');
var SecurityService = require('./service/SecurityService');
var SNSService = require('./service/SNSService');
var FirebaseService = require('./service/FirebaseService');
var SystemService = require('./service/SystemService');
var firebaseService = new FirebaseService();

exports.handle = function (event, context, callback) {
    const userId = event.principalId;
    context.callbackWaitsForEmptyEventLoop = false;
    const securityService = new SecurityService();
    const body = event.body;
    const id = event.body.id;
    const first_name = body && body.first_name ? body.first_name : '';
    const last_name = body && body.last_name ? body.last_name : '';
    const email = body && body.email ? body.email : '';
    const password = body && body.password ? body.password : '';
    const phone_number = body && body.phone_number ? body.phone_number : 0;
    const status = body && body.status ? body.status : '';
    const role = body && body.role ? body.role : '';
    const bid_attempts = body && body.bid_attempts ? body.bid_attempts : undefined;
    let needSystemUser = false;
    let updateFields = {};
    let isSystem = true;
    const systemService = new SystemService(iocContainer.models.users);

    if (first_name.length > 0) {
        updateFields.first_name = first_name;
    }
    if (last_name.length > 0) {
        updateFields.last_name = last_name;
    }
    if (email.length > 0) {
        updateFields.email = email;
    }
    if (phone_number > 0) {
        updateFields.phone_number = phone_number;
    }
    if (status.length > 0) {
        updateFields.status = status;
    }
    if (role.length > 0) {
        switch (role) {
            case 'Member':
                updateFields.role = 'Member';
                break;
            case 'Vendor':
                updateFields.role = 'Vendor';
                break;
            case 'Distributor':
                updateFields.role = 'Distributor';
                break;
            case 'Admin':
                needSystemUser = true;
                updateFields.role = 'Admin';
                break;
            case 'System':
                needSystemUser = true;
                updateFields.role = 'System';
                break;
            default:
                break;
        }
    }

    if (bid_attempts != undefined) {
        updateFields.bid_attempts = bid_attempts;
    }
    if (password.length > 0) {
        updateFields.password = securityService.hash(password);
    }

    systemService.isSystemUser(userId).then(function (isSystemResult) {
        isSystem = isSystemResult;
        if (needSystemUser && !isSystemResult) {
            throw new Error('Not System User');
        } else {
            return iocContainer.models.users.find({
                where: {
                    id: id
                }
            });
        }
    }).then(function (user) {
        if (user.role == 'System' && !isSystem) {
            throw new Error('Not System User');
        } else {
            return user.updateAttributes(updateFields);
        }
    }).then(function (user) {
        user = user.toJSON();
        delete user.access_token;
        delete user.refresh_token;
        delete user.confirm_token;
        delete user.reset_token;
        delete user.term_condition;
        delete user.password;
        if (user.status == 'Member') {
            return firebaseService.write('user', user.id, user).then(function (result) {
                return callback(null, user);
            })
        } else {
            return callback(null, user);
        }
    }).catch(function (e) {
        if (!isSystem) {
            return callback(null, {
                statusCode: 403,
                message: 'No está autorizado para realizar esta tarea'
            });
        } else {
            console.log('Edit User ERROR', e);
            callback(null, {
                statusCode: 403,
                message: 'Error de actualización del usuario'
            });
        }
    });
}
