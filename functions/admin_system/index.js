'use strict';
var iocContainer = require('./ioContainer');
exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    iocContainer.models.system.find({
        where: {
            id: 1
        }                
    }).then( function (system) {
            callback(null, system);
        }).catch( function (e) {
            console.log('system GET ERROR', e);
            callback(null, {
                statusCode: 404,
                message: 'Sistema no encontrado'
            });
        });
}
