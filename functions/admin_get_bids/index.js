'use strict';
var iocContainer = require('./ioContainer');
var lodash = require('lodash');
var PaginationService = require('./service/PaginationService');
var SystemService = require('./service/SystemService');
exports.handle = function (event, context, callback) {
    const userId = event.principalId;
    context.callbackWaitsForEmptyEventLoop = false;
    const pagination = new PaginationService(event.query.page, event.query.numItems);
    const code = event && event.query && event.query.code ? event.query.code : null;
    let isSystem = true;
    const systemService = new SystemService(iocContainer.models.users);

    let where = {};
    let query = {
        attributes: [[iocContainer.db.fn('COUNT', iocContainer.db.col('id')), 'num']]
    };

    if (code) {
        where.code = code;
    }

    if (!lodash.isEmpty(where)) {
        query.where = where;
    }
    systemService.isSystemUser(userId).then(function (isSystemResult) {
        if (!isSystemResult) {
            isSystem = false;
            throw new Error('Not System User');
            return false;
        } else {
            return iocContainer.models.bid_cards.findAll(query);
        }
    }).then(
        function (countResult) {
            if (countResult.length > 0) {
                pagination.setCount(countResult[0].dataValues.num);
                delete query.attributes;
                query.offset = pagination.getOffset();
                query.limit = pagination.getLimit();
                return iocContainer.models.bid_cards.findAll(query);
            } else {
                callback(null, {
                    statusCode: 200,
                    items: pagination.getItems(),
                    page: pagination.getPage(),
                    num_items: pagination.getCount(),
                    num_pages: pagination.getNumPages()
                });
            }
        }).then(function (bids) {
            pagination.setItems(bids);
            callback(null, {
                statusCode: 200,
                items: pagination.getItems(),
                page: pagination.getPage(),
                num_items: pagination.getCount(),
                num_pages: pagination.getNumPages()
            });
        }).catch(function (e) {
            if (!isSystem) {
                return callback(null, {
                    statusCode: 403,
                    message: 'You are not authorize to do this task'
                });
            } else {
                console.log('Bid Cards GET ERROR', e);
                callback(null, {
                    statusCode: 404,
                    message: 'Bid Cards not found'
                });
            }
        });
}
