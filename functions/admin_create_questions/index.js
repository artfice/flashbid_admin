'use strict';
var iocContainer = require('./ioContainer');
var SecurityService = require('./service/SecurityService');
var SNSService = require('./service/SNSService');
exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    const securityService = new SecurityService();
    const body = event.body;
        const question = body && body.question ? body.question : ''; 
        const answer = body && body.answer ? body.answer : '';
        if(question.length < 1) {
            return callback(null, {
                statusCode: 403,
                message: 'Se requiere una pregunta.'
            });
        } 
        if(answer.length < 1) {
            return callback(null, {
                statusCode: 403,
                message: 'La respuesta es requerida'
            });
        } 
        iocContainer.models.questions.create({
           question: question,
           answer: answer
        }).then( function (questions) {
            return callback(null, questions);
        }).catch( function (e) {
            console.log('Question POST ERROR', e);
            callback(null, {
                statusCode: 403,
                message: 'La creacion de la pregunta no fue realizada.'
            });
        });
}
