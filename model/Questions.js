module.exports = function (sequelize, DataTypes) {
return sequelize.define('questions', {
  question:       DataTypes.STRING,
  answer:       DataTypes.TEXT
}, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'questions'
    });
};