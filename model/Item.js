module.exports = function (sequelize, DataTypes) {
  return sequelize.define('items', {
  title:       DataTypes.STRING,
  image:       DataTypes.STRING,
  secondary_image:       DataTypes.STRING,
  bid_amount:       DataTypes.INTEGER,
  type:       DataTypes.ENUM('a', 'l'),
  initial_bid:       DataTypes.INTEGER,
  item_worth:       DataTypes.INTEGER,
  final_bid:       DataTypes.INTEGER,
  ticket_total: DataTypes.INTEGER,
  ticket_sold: DataTypes.INTEGER,
  winner_id: DataTypes.INTEGER,
  room_id: DataTypes.INTEGER, 
  winner_code:       DataTypes.STRING,
  bid_history:       DataTypes.TEXT,
  status:       DataTypes.ENUM('Lost','Draft', 'Active', 'Flash', 'Complete', 'Claimed'),
  created_at: DataTypes.DATE,
  updated_at: DataTypes.DATE,
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'items'
});
};