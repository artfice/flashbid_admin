#!/bin/bash

folder=$1
mkdir "./functions/$folder"
rm -rf "./functions/${folder}/node_modules"
rm -rf "./functions/${folder}/ioContainer.js"
rm -rf "./functions/${folder}/service"
rm -rf "./functions/${folder}/model"
rm -rf "./functions/${folder}/.env"

cp -R node_modules "./functions/${folder}/node_modules"
cp ioContainer.js "./functions/${folder}/ioContainer.js"
cp -R service "./functions/${folder}/service"
cp -R model "./functions/${folder}/model"
cp .env "./functions/${folder}/.env"
cp package.json "./functions/${folder}/package.json"
echo '{"name": "test","description": "","runtime": "nodejs4.3","memory": 128,"timeout": 5,"role": "arn:aws:iam::954804258659:role/aws-nodejs-dev-IamRoleLambdaExecution-7LS6GPL2FJSJ"}' > "functions/$folder/function.json"
echo "'use strict'; var iocContainer = require('./ioContainer'); exports.handle = function(event, context, callback) { context.callbackWaitsForEmptyEventLoop = false; callback(null, event); }" > "functions/$folder/index.js"
