'use strict';

var dotenv = require('dotenv');

dotenv.config();

var Sequelize = require('sequelize');
var fs = require('fs');
var path = require('path');

let sequelize = new Sequelize(
    process.env.DATABASE,
    process.env.USERNAME,
    process.env.PASSWORD, {
        host: process.env.HOST,
        dialect: process.env.DATABASE_DIALECT,
        port: process.env.PORT,
        // pool: {
        //     max: process.env.DATABASE_MAX_POOL,
        //     min: process.env.DATABASE_MIN_POOL,
        //     idle: process.env.DATABASE_IDLE
        // },
        logging: false,
        maxConcurrentQueries: 100,
        pool: { maxConnections: 15, minConnections:0, maxIdleTime: 1000},
        define: {
            timestamps: false
        }
    });

const models = {};
const modelPath = __dirname + '/model';

fs.readdirSync(modelPath).filter(
function(file) { 
    return (file.indexOf('.') !== 0) && (file.slice(-3) === '.js');
}).forEach(function(file) {
    const model = sequelize['import'](path.join(modelPath, file));
    models[model.name] = model;
});

module.exports = {
    db: sequelize,
    models: models,
    isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.SERIALIZABLE
};