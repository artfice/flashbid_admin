#!/bin/bash

folder=$1

rm -rf "./functions/${folder}/node_modules"
rm -rf "./functions/${folder}/ioContainer.js"
rm -rf "./functions/${folder}/service"
rm -rf "./functions/${folder}/model"
rm -rf "./functions/${folder}/.env"