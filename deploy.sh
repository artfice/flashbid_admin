#!/bin/bash

folder=$1
cd functions
find . -name "node_modules" -exec rm -rf '{}' +
rm -rf "./${folder}/ioContainer.js"
rm -rf "./${folder}/service"
rm -rf "./${folder}/model"
rm -rf "./${folder}/.env"
cd ..
cp -R node_modules "./functions/${folder}/node_modules"
cp ioContainer.js "./functions/${folder}/ioContainer.js"
cp -R service "./functions/${folder}/service"
cp -R model "./functions/${folder}/model"
cp .env "./functions/${folder}/.env"
apex deploy $1